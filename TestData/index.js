
const Config = require('./config');
const Logger = require('./logger');
const { ProcessIntentffModel, TransformGraphModel } = require('./ProcessInitiater');

var processToExecute = 0;
var nlpIntentModelToExecute = 'Health01';
var nlpIntentNameToExecute = 'nlp005';

var transformSampleJson = '1';

if (process.argv && process.argv.length > 2) {
    if (process.argv[2]!='undefined' && process.argv[2].toLowerCase()=='process') {
        nlpIntentModelToExecute = process.argv[3];
        nlpIntentNameToExecute = process.argv[4];
        processToExecute = 0;
    }

    if (process.argv[2]!='undefined' && process.argv[2].toLowerCase()=='dmodeltransform') {
        transformSampleJson = process.argv[3];
        processToExecute = 1;
    }

    if (process.argv[2]!='undefined' && process.argv[2].toLowerCase()=='intentffmodeltransform') {
        processToExecute = 2;
    }
}

var sessionid = Logger.Initialize();

switch(processToExecute) {
    case 0: Logger.Log(sessionid, '** Processing IntentffModel...');
            ProcessIntentffModel.Initialize(Config, nlpIntentModelToExecute, nlpIntentNameToExecute, Logger, sessionid)
                .then(x => { 
                    Logger.Log(sessionid, 'initialized...'); 
                    ProcessIntentffModel.ProcessIntentModel(); 
                }); 
            break;

    case 1: Logger.Log(sessionid, '** Transforming DomainModel Graph to DomainModel...');
            TransformGraphModel.GetDomainModel();    
            break;
    case 2: Logger.Log(sessionid, '** Transforming IntentffModel Graph IntentffModel...');
            TransformGraphModel.GetIntentffModel();  
            break;        
}

