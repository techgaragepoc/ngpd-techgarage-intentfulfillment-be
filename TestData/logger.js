class Logger {
    constructor() {
        this.logs = [];
    }

    Initialize() {
        return (new Date()).toString();
    }

    Log(sessionid, message, level=0) {
        var messagestr = message;
        if (message.step &&  message.step!='undefined' && message.message && message.message!='undefined') {
            messagestr = message.step + '...' + message.message;
        }
        console.log('session: ' + sessionid + ' | Message => ' + level + ':' + messagestr);
        this.logs.push({
            session: sessionid,
            message: message
        });
    }
}

module.exports = new Logger();