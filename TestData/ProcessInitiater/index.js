const ProcessIntentffModel = require('./processintentffmodel');
const TransformGraphModel = require('./TransformGraphModel');

module.exports = {
    ProcessIntentffModel,
    TransformGraphModel
}
