const TranformGraphModelHandler = require('../../Processor/IntentFulfillmentModel/transformgraphmodel.handler');
const sampleJsonDomainModelGraph = require('../Database/DomainModelGraph/sample1.json');

class TransformGraphModel {
    constructor() {

    }

    GetDomainModel() {
        return TranformGraphModelHandler.GetDomainModel(sampleJsonDomainModelGraph)
               .then(nextnodes => console.log('next nodes => ', nextnodes));
    }

    GetIntentffModel() {
        return TranformGraphModelHandler.GetIntentffModel(sampleJsonDomainModelGraph)
                .then(nextnodes => console.log('next nodes => ', nextnodes));
    }
}

module.exports = new TransformGraphModel();