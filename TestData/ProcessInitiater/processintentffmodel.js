
const IntentFulfillmentIdentifierService = require('../../Processor/IntentFulfillmentIdentifier/intentfulfillment.identifier.service');

class ProcessIntentffModel {
    constructor() {
        this.Config = {};
        this.nlpIntentModelToExecute = 'Health01';
        this.nlpIntentNameToExecute = 'nlp005';
        this.SessionId = '';
        this.Logger = { Log: function(message) {
            console.log('logger not assigned properly - ' + message);
        }};

    }

    Initialize(config, nlpmodel, nlpintent, logger, sessionId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.Config = config;
            if (nlpmodel != null && nlpmodel != 'undefined' && nlpmodel != '') {
                self.nlpIntentModelToExecute = nlpmodel;
            }
            if (nlpintent != null && nlpintent != 'undefined' && nlpintent != '') {
                self.nlpIntentNameToExecute = nlpintent;
            }

            self.SessionId = sessionId;
            self.Logger = logger;

            resolve(true);
        });
    }

    ProcessIntentModel() {
        var self = this;

        //0. Get parsed intent tree in memory
        const nlpTree = GetNLPTreeFromTestData(self.nlpIntentNameToExecute);
        console.log('nlpTree =>', nlpTree);

        if (!nlpTree || nlpTree == 'undefined') {
            console.log('Stopped: As Test Sample not found');
        }
        else {



            FetchDefinedModelsFromDatabase(self.Config, true).then(metadata => {
                console.log('metadata =>', metadata);

                //1. Get the metadata from database (save thru Admin UI)
                IntentFulfillmentIdentifierService.Initialize(metadata.DomainModels, metadata.IntentFulfillmentModels
                    , metadata.AggregateFunctions, metadata.Synonyms, metadata.NLPIntentFulfillmentModelMappings
                    , metadata.NodeDatasetMappings, self.Config.GetAdminAPIEndPoint(), self.Logger, self.SessionId)
                    .then(x => {

                        //2. Get intent seq. based on matching criteria
                        IntentFulfillmentIdentifierService.GetMappedIntentFulfillmentModel(self.nlpIntentModelToExecute, self.nlpIntentNameToExecute)
                            .then(intentFulfillmentModel => {

                                //console.log('mapped intentFulfillmentModel =>', intentFulfillmentModel);
                                // no mapping found
                                if (intentFulfillmentModel == null || intentFulfillmentModel == 'undefined') {
                                    // 2 b. Get best possible match
                                    IntentFulfillmentIdentifierService.GetIntentFulfillmentModel(nlpTree)
                                        .then(matchedIntentffModel => {

                                            //3. process the model
                                            processIntentFulfillmentModel(IntentFulfillmentIdentifierService, nlpTree, intentFulfillmentModel, printOutput);
                                        }); // GetIntentModel
                                }
                                else {
                                    //3. process the model
                                    processIntentFulfillmentModel(IntentFulfillmentIdentifierService, nlpTree, intentFulfillmentModel, printOutput);
                                } //if mapping found

                            }); // GetMappedIntentModel

                    }); // Init
            }); // FetchDefinedModelsFromDatabase
        }
    }
}

module.exports = new ProcessIntentffModel();



function processIntentFulfillmentModel(IntentFulfillmentIdentifierService, nlpTree, intentFulfillmentModel, callback) {
    // console.log('Start Processing Model...');
    //3. process the model
    IntentFulfillmentIdentifierService.ProcessIntentFulfillmentModel(nlpTree, intentFulfillmentModel)
        .then(result => {
            callback(result);
        }); // ProcessIntentModel
}

function printOutput(result) {
    /*
        30: black; 31:red; 32:green; 33:yellow; 34:blue; 35:magenta; 36:cyan; 37:white
    */

    console.log('\x1b[33m', '**************** Result (Processing Ends) ******************', '\x1b[0m');
    if (result) {
        if (result.processedintentmodel != null && result.processedintentmodel != 'undefined') {
            console.log('\x1b[36m', 'processed Fulfillment model =>', result.processedintentmodel, '\x1b[0m');
        }

        if (result.resultnode != null && result.resultnode != 'undefined') {
            console.log('\x1b[33m', '\x1b[1m', 'result node data =>', JSON.stringify(result.resultnode), '\x1b[22m', '\x1b[0m');
        }

        if (result.resultrows != null && result.resultrows != 'undefined') {
            console.log('\x1b[32m', '\x1b[1m', 'result row(s) =>', JSON.stringify(result.resultrows), '\x1b[22m', '\x1b[0m');
            // console.log('\x1b[32m', '\x1b[1m', 'result =>', JSON.stringify(result.resultnode.properties.output), '\x1b[22m', '\x1b[0m');
        }

        console.log('\x1b[31m', 'error =>', JSON.stringify(result.error), '\x1b[0m');
    }
    console.log('\x1b[33m', '*****************************************************', '\x1b[0m');
}

function GetNLPTreeFromTestData(sampleid) {
    const nlpsamples = require('../nlp/nlp.output.json');

    for (var i = 0; i < nlpsamples.length; i++) {
        if (nlpsamples[i].sampleid == sampleid) {

            return nlpsamples[i].nlptree;
        }
    }

    return null;
}



function FetchDefinedModelsFromDatabase(config, isRestApi) {
    var metadata = {};

    if (isRestApi) {
        const RestApiToJson = require('../Database/ZService/restapiToJson');

        return RestApiToJson.Initialize(config.GetAdminAPIEndPoint())
            .then(x => { return RestApiToJson.GetDomainModels().then(models => metadata["DomainModels"] = models) })
            .then(x => { return RestApiToJson.GetSynonyms().then(syn => metadata["Synonyms"] = syn) })
            .then(x => { return RestApiToJson.GetIntentFulfillmentModels().then(imodels => metadata["IntentFulfillmentModels"] = imodels) })
            .then(x => { return RestApiToJson.GetFunctionMetaData().then(funcs => metadata["AggregateFunctions"] = funcs) })
            .then(x => { return RestApiToJson.GetNLPIntentFulfillmentModelMappings().then(mapping => metadata["NLPIntentFulfillmentModelMappings"] = mapping) })
            .then(x => { return RestApiToJson.GetNodeDatasetMappings().then(dsmapping => metadata["NodeDatasetMappings"] = dsmapping) })
            .then(x => {
                return metadata;
            });
    }
    else {
        const MongoToJson = require('../Database/ZService/MongoToJson');

        return MongoToJson.Initialize(config.GetMongoURI(), config.GetMongoDBName())
            .then(x => { return MongoToJson.GetDomainModels().then(models => metadata["DomainModels"] = models) })
            .then(x => { return MongoToJson.GetSynonyms().then(syn => metadata["Synonyms"] = syn) })
            .then(x => { return MongoToJson.GetIntentFulfillmentModels().then(imodels => metadata["IntentFulfillmentModels"] = imodels) })
            .then(x => { return MongoToJson.GetFunctionMetaData().then(funcs => metadata["AggregateFunctions"] = funcs) })
            .then(x => { return MongoToJson.GetNLPIntentFulfillmentModelMappings().then(mapping => metadata["NLPIntentFulfillmentModelMappings"] = mapping) })
            .then(x => { return MongoToJson.GetNodeDatasetMappings().then(dsmapping => metadata["NodeDatasetMappings"] = dsmapping) })
            .then(x => {
                return metadata;
            });
    }
}