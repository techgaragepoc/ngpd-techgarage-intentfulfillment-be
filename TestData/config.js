class Configuration {
    constructor() {

    }

    GetMongoURI(){
        return 'mongodb://localhost:27017';
    }

    GetMongoDBName(){
        return 'mladmindb'; //'DialogueScale';
    }

    GetAdminAPIEndPoint() {
        return 'http://localhost:5001/api';
    }


}

module.exports = new Configuration();