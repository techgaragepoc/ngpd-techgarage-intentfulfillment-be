const JsonToMongoDB = require('./JsonToMongo');
const MongoToJson = require('./MongoToJson');
const config = require('../../config');

const jsonToMongo = true;
const mongoToJson = false;

if (jsonToMongo) {
    JsonToMongoDB.Initialize(config.GetMongoURI(), config.GetMongoDBName())
    .then(x=> { JsonToMongoDB.UpdateCollections() });
}

if (mongoToJson) {
    MongoToJson.Initialize(config.GetMongoURI(), config.GetMongoDBName())
    .then(x => {

        MongoToJson.GetFunctionMetaData().then(
            data => { console.log('Function Meta Data =>', data); },
            err => { console.log('Function Meta Data error =>', err) }
        );
    
        MongoToJson.GetDomainModels().then(
            data => { console.log('Domain Models =>', data.length); },
            err => { console.log('Domain Models error =>', err) }
        );
    
        MongoToJson.GetSynonyms().then(
            data => { console.log('Synonyms =>', data.length); },
            err => { console.log('Synonyms error =>', err) }
        );
    
        MongoToJson.GetIntentModels().then(
            data => { console.log('Intent Models =>', data.length); },
            err => { console.log('Intent Models error =>', err) }
        );

    });

}