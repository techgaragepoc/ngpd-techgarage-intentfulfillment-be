const request = require('request');

class RestApiToJson {
    constructor() {
        this.AdminAPIEndPoint = '';
    }

    Initialize(adminapiendpoint) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.AdminAPIEndPoint = adminapiendpoint;
            resolve(true);
        });
    }

    GetDomainModels() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/domainmodel/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) } );
    }

    GetFunctionMetaData() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/functions/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) });
    }

    GetIntentFulfillmentModels() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/model/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) });
    }

    GetNodeDatasetMappings() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/nodedatasetmappings/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) });
    }

    GetNLPIntentFulfillmentModelMappings() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/nlpintentfulfillmentmappings/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) });
    }

    GetSynonyms() {
        const url = this.AdminAPIEndPoint + '/intentfulfillment/synonyms/';
        return this.CallRestAPI(url).then(result => {return this.ReturnResult(result) });
    }

    ReturnResult(result) {
        return new Promise(function(resolve,reject) { 
            resolve(result.data); 
        });
    
    }

    CallRestAPI(url) {
        var self = this;

        return new Promise(function (resolve, reject) {
            var options = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
                json: true,
            };

            // console.log('request url options =>', url, options);
            request(url, options, function (err, response, body) {
                // console.log('reponse from ec...',JSON.parse(body));
                if (err) {
                    resolve({ error: err, data: null });
                }
                else {
                    console.log('statuscode=', response.statusCode);
                    if (response.statusCode >= 400) {
                        // callback(response.statusMessage, null);
                        resolve({ error: response.statusMessage, data: null });
                    }
                    else {
                        var result = self.AsObject(body);
                        resolve({ error: null, data: result.data });
                        // callback(null, result.data);
                    }
                }
            });
        }); // Promise

    }

    AsObject(body) {
        var bodyobj = {};

        if (body && typeof body !== "object") {
            bodyobj = JSON.parse(body);
        } else {
            bodyobj = body;
        }

        return bodyobj;
    }
}

module.exports = new RestApiToJson();