
const MongoClient = require('mongodb').MongoClient;


class mongoDBOperations {
    constructor() {
        this.MongoURI = '';
        this.MongoDBName = '';
    }

    Initialize(mongoUri, mongoDbname) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.MongoURI = mongoUri;
            self.MongoDBName = mongoDbname;
            resolve(true);
        });
    }


    GetDocuments(collectionname, filterquery) {
        var self = this;
        return new Promise(function (resolve, reject) {

            // Use connect method to connect to the server
            MongoClient.connect(self.MongoURI, function (err, client) {

                if (err) {
                    reject(err);
                }
                else {
                      console.log("GetDocuments %s", collectionname);

                    const db = client.db(self.MongoDBName);
                    const collection = db.collection(collectionname);
                    var result = [];

                    collection.find(filterquery, function (docerr, docs) {
                        if (docerr) {
                            client.close();
                          //  console.log('error...', docerr);
                            reject(docerr);
                        } else {
                            result = docs.toArray();
                            client.close();
                           // console.log('data...', docs.toArray());
                            resolve(result);
                        }
                    });
                }
            });

        });


    }


    InsertDocuments(collectionname, documents, callback) {
        var self = this;
        // Use connect method to connect to the server
        MongoClient.connect(self.MongoURI, function (err, client) {

            if (err) {
                callback(err, null);
            }
            else {
                //  console.log("Connected successfully to server");
                if (documents && documents.length>0) {
                    const db = client.db(self.MongoDBName);
                    const collection = db.collection(collectionname);
    
                    collection.insertMany(documents, function (cerr, result) {
                        callback(cerr, result);
                        client.close();
                    });
                }
                else {
                    callback({"error": "documents are either undefined or null or empty"}, null);
                }
            }
        });
    }

    RemoveDocuments(collectionname, filterquery, callback) {
        var self = this;
        // Use connect method to connect to the server
        MongoClient.connect(self.MongoURI, function (err, client) {

            if (err) {
                callback(err, null);
            }
            else {
                //  console.log("Connected successfully to server");

                const db = client.db(self.MongoDBName);
                const collection = db.collection(collectionname);

                collection.deleteMany(filterquery, function (cerr, result) {
                    callback(cerr, result);
                    client.close();
                });
            }
        });
    }

    RefreshCollection(collectionname, documents, callback) {
        const self = this;

        self.RemoveDocuments(collectionname, {}, function (remerr, remresult) {

            if (remerr) {
                console.log('Error occured while removing documents from %s : ', collectionname, remerr);
            }
            else {
                console.log('%s : documents removed, inserting documents', collectionname);
                self.InsertDocuments(collectionname, documents, function (err, result) {
                    console.log('Refresh Collection for %s  => (err)', collectionname, err);
                    console.log('Refresh Collection for %s  => (result)', collectionname, result);
                    callback(err, result);
                });
            }

        });
    }

}

module.exports = new mongoDBOperations();