const dbOperation = require('./mongoDBOperations')


class MongoToJson {
    constructor() {

    }

    Initialize(mongoUri, mongoDbname) {
        return dbOperation.Initialize(mongoUri, mongoDbname);
    }

    GetDomainModels() {
       const collection =  ('DomainModels').toLowerCase();
       return dbOperation.GetDocuments(collection,{});
    }

    GetFunctionMetaData() {
        const collection =  ('Functions').toLowerCase();
        return dbOperation.GetDocuments(collection,{});
    }

    GetIntentFulfillmentModels() {
        const collection =  ('IntentFulfillmentModels').toLowerCase();
        return dbOperation.GetDocuments(collection,{});
    }

    GetNodeDatasetMappings() {
        const collection =  ('NodeDatasetMappings').toLowerCase();
        return dbOperation.GetDocuments(collection,{});
    }

    GetNLPIntentFulfillmentModelMappings() {
        const collection =  ('NlpIntentFulfillmentMappings').toLowerCase();
        return dbOperation.GetDocuments(collection,{});
    }

    GetSynonyms() {
        const collection =  ('Synonyms').toLowerCase();
        return dbOperation.GetDocuments(collection,{});
    }


}

module.exports = new MongoToJson();