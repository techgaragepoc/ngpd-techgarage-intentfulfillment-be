const dbOperation = require('./mongoDBOperations')

const { DomainModels } = require('../DomainModel/');
const { AggregateFunctions } = require('../Function/');
const { Synonyms } = require('../Synonyms/');
const { IntentFulfillmentModel } = require('../IntentFulfillmentModel/');
const { NlpIntentFulfillmentMapping } = require('../NlpIntentFulfillmentMapping/');
const { NodeDataSetMapping } = require('../NodeDatasetMapping/');

class JsonToMongoDB {
    constructor() {

    }

    Initialize(mongoUri, mongoDbname) {
        return dbOperation.Initialize(mongoUri, mongoDbname);
    }

    UpdateCollections() {

        const self = this;

        var collectionname = ('Functions').toLowerCase();
        console.log('*********** %s  Start ********************', collectionname);
        dbOperation.RefreshCollection(collectionname, AggregateFunctions, function (err, result) {
            console.log('*********** %s  Complete ********************', collectionname);
        }); //Functions

        collectionname = ('DomainModels').toLowerCase();
        console.log('*********** %s  Start ********************', collectionname);
        dbOperation.RefreshCollection(collectionname, DomainModels, function (derr, dresult) {
            console.log('*********** %s  Complete ********************', collectionname);

            collectionname = ('Synonyms').toLowerCase();
            console.log('*********** %s  Start ********************', collectionname);
            dbOperation.RefreshCollection(collectionname, Synonyms, function (serr, sresult) {
                console.log('*********** %s  Complete ********************', collectionname);

                collectionname = ('IntentFulfillmentModels').toLowerCase();
                console.log('*********** %s  Start ********************', collectionname);
                dbOperation.RefreshCollection(collectionname, IntentFulfillmentModel, function (ierr, iresult) {
                    console.log('*********** %s  Complete ********************', collectionname);


                    collectionname = ('NodeDatasetMappings').toLowerCase();
                    console.log('*********** %s  Start ********************', collectionname);
                    dbOperation.RefreshCollection(collectionname, NodeDataSetMapping, function (ierr, iresult) {
                        console.log('*********** %s  Complete ********************', collectionname);


                        collectionname = ('NlpIntentFulfillmentMappings').toLowerCase();
                        console.log('*********** %s  Start ********************', collectionname);
                        dbOperation.RefreshCollection(collectionname, NlpIntentFulfillmentMapping, function (maperr, mapresult) {
                            console.log('*********** %s  Complete ********************', collectionname);

                        }); //NLPIntentModelMapping

                    }); // NodeDatasetMappings

                }); //IntentFulfillmentModel
            }); //Synonyms
        }); //DomainModels

    }

}

module.exports = new JsonToMongoDB();