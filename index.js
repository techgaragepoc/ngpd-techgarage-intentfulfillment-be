
const intentFulfillmentIdentifierService = require('./Processor/IntentFulfillmentIdentifier/intentfulfillment.identifier.service');


class IntentFulfillmentAPI {

    constructor() {
        this.IntentFulfillmentIdentifierService = intentFulfillmentIdentifierService;
        this.ConnectorAPIEndPoint = '';
        this.IsInitialized = false;

        this.Logger = { Log: function(sessionid, message) {
            console.log('logger not assigned properly - ' + message);
        }};
    }

    //1. Initialize the metadata from database (saved thru Admin UI)
    Initialize(domainModels, intentFulfillmentModels,
         aggregateFunctionsMetaData, synonyms, nlpIntentFulfillmentModelMappings,
         nodedatasetmapping, connectorAPIEndPoint, logger, sessionId) {

        this.ConnectorAPIEndPoint = connectorAPIEndPoint;
        this.IsInitialized = true;
        this.Logger = logger;
        this.SessionId = sessionId;

        return this.IntentFulfillmentIdentifierService.Initialize(domainModels, intentFulfillmentModels
            , aggregateFunctionsMetaData, synonyms, nlpIntentFulfillmentModelMappings, nodedatasetmapping,
             connectorAPIEndPoint, this.Logger, this.SessionId);

    }

    //2. Get intent seq. based on matching criteria
    IdentifyModel(nlpIntentModelToExecute, nlpIntentNameToExecute, nlpTree) {
        var self = this;
        return new Promise(function (resolve, reject) {

            if (!self.IsInitialized) {
                console.log('Exit as api is not initialized');
                resolve(null);
            }
            else {
                self.IntentFulfillmentIdentifierService.GetMappedIntentFulfillmentModel(nlpIntentModelToExecute, nlpIntentNameToExecute)
                    .then(intentFulfillmentModel => {

                        //console.log('mapped intentFulfillmentModel =>', intentFulfillmentModel);
                        // no mapping found
                        if (intentFulfillmentModel == null || intentFulfillmentModel == 'undefined') {
                            // 2 b. Get best possible match
                            self.IntentFulfillmentIdentifierService.GetIntentFulfillmentModel(nlpTree)
                                .then(matchedIntentffModel => {
                                    resolve(matchedIntentffModel);
                                }); // GetIntentModel
                        }
                        else {
                            resolve(intentFulfillmentModel);
                        } //else (mapping found)
                    }); // GetMappedIntentModel

            }
        }); // Promise
    }

    //3. process the model
    ExecuteModel(intentFulfillmentModel, nlpTree) {
        var self = this;
        return new Promise(function (resolve, reject) {

            if (!self.IsInitialized) {
                console.log('Exit as api is not initialized');
                resolve(null);
            }
            else {
                self.IntentFulfillmentIdentifierService.ProcessIntentFulfillmentModel(nlpTree, intentFulfillmentModel)
                    .then(result => {
                        resolve(result);
                    }); // ProcessIntentFulfillmentModel
            }
        }); // Promise
    }
}

module.exports = new IntentFulfillmentAPI();


function printOutput(result) {
    /*
        30: black; 31:red; 32:green; 33:yellow; 34:blue; 35:magenta; 36:cyan; 37:white
    */

    console.log('\x1b[33m', '**************** Result (Processing Ends) ******************', '\x1b[0m');
    if (result) {
        console.log('\x1b[36m', 'processed Fulfillment model =>', result.processedintentmodel, '\x1b[0m');
        console.log('\x1b[33m', '\x1b[1m', 'result node data =>', JSON.stringify(result.resultnodedata), '\x1b[22m', '\x1b[0m');
        console.log('\x1b[32m', '\x1b[1m', 'result =>', JSON.stringify(getresultrows(result.resultnodedata)), '\x1b[22m', '\x1b[0m');
        console.log('\x1b[31m', 'error =>', JSON.stringify(result.error), '\x1b[0m');
    }
    console.log('\x1b[33m', '*****************************************************', '\x1b[0m');
}

function getresultrows(resultnodedata) {
    var result = [];
    if (resultnodedata != null && resultnodedata != 'undefined' && resultnodedata.length > 0) {
        for (var i = 0; i < resultnodedata.length; i++) {
            result.push(resultnodedata[i].value);
        }
    }
    return result;
}
