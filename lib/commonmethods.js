class CommonMethods {

    constructor() {

    }

    IsNull(value) {

        if (value == null || value == '' || value == 'undefined' ||
            (typeof value === 'object' && this.IsEmpty(value))) {
            return true;
        }
        else {
            return false;
        }
    }

    IsEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    ReplaceAll(source, search, replacement) {
        return source.replace(new RegExp(search, 'g'), replacement);
    }

    IsArray(value) {
        return value && value!='undefined' && typeof value === 'object' && value.constructor === Array;
    }
    
    AddPropertyValuePair(sourceObj, propvaluetoadd) {

        if (typeof propvaluetoadd === 'object' && !this.IsEmpty(propvaluetoadd)) {

            for (var i = 0; i < Object.keys(propvaluetoadd).length; i++) {
                var key = Object.keys(propvaluetoadd)[i];
                sourceObj[key] = propvaluetoadd[key];
            }
        }

        return sourceObj;
    }

    AsObject(body) {
        var bodyobj = {};

        if (body && typeof body !== "object") {
            bodyobj = JSON.parse(body);
        } else {
            bodyobj = body;
        }

        return bodyobj;
    }

    GetMatchedObject(sourcearray, sourceprop, value) {
        for (var i = 0; i < sourcearray.length; i++) {
            if (sourcearray[i] != null && sourcearray[i] != 'undefined'
                && sourcearray[i][sourceprop] == value) {
                return sourcearray[i];
            }
        }

        return null;
    }

    GetDataRowValue(data, previousNodeDataRows, nodeid) {

        if (data && data != 'undefined') {
            //console.log('previousNodeDataRows, nodeid =>', previousNodeDataRows, nodeid);
            var rowno = 0;
            //get the linked rowno
            if (previousNodeDataRows != null && previousNodeDataRows.length > 0) {
                for (var lnkCtr = 0; lnkCtr < previousNodeDataRows.length; lnkCtr++) {
                    if (previousNodeDataRows[lnkCtr].nodeid == nodeid) {
                        rowno = previousNodeDataRows[lnkCtr].rowno;
                        break;
                    }
                }
            }


            for (var i = 0; i < data.length; i++) {
                if (data[i] != null && data[i].rowno != null && data[i].rowno != 'undefined' && data[i].rowno == rowno) {
                   // console.log('data[i].value =>', data[i].value);
                    return data[i].value;
                }
            }
        }

        return null;
    }

}

module.exports = new CommonMethods();

