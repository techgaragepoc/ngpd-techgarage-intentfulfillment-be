const commonMethods = require('../../lib/commonmethods');
const nodedataProcessor = require('../DataProcessor/nodedata.processor');

class DomainModelProcessor {

    constructor() {
        this.NodeDataProcessor = nodedataProcessor;
        this.CommonMethods = commonMethods;

        this.DomainModels = [];
        this.Synonyms = [];
        this.NodeDatasetMappings = [];

        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };

    }

    InitializeDataSets(domainmodels, synonyms, nodedatasetmapping, connectorAPIEndPoint, logger, sessionId) {
        this.DomainModels = domainmodels;
        this.Synonyms = synonyms;
        this.NodeDatasetMappings = nodedatasetmapping;
        this.NodeDataProcessor.InitializeDataSets(connectorAPIEndPoint, logger, sessionId);
        this.Logger = logger;
        this.SessionId = sessionId;
    }

    GetMatchedDomainModelNode(word) {
        // console.log('Domain Model length =>', this.DomainModels.length);
        for (var modelCtr = 0; modelCtr < this.DomainModels.length; modelCtr++) {
            var model = this.DomainModels[modelCtr];

            //for each node of model
            if (model.nodes && model.nodes != 'undefined') {
                for (var nodeCtr = 0; nodeCtr < model.nodes.length; nodeCtr++) {
                    var modelNode = model.nodes[nodeCtr];

                    //if model name matches
                    if (modelNode.label.toLowerCase() == word.toLowerCase()) {
                        return modelNode;
                    }

                    //if attribute matches
                    const nodeprop = this.GetNodeProperty(model, modelNode.nodeid);
                    if (nodeprop.attributes && nodeprop.attributes != 'undefined') {
                        for (var attrCtr = 0; attrCtr < nodeprop.attributes.length; attrCtr++) {
                            var nodeAttribute = nodeprop.attributes[attrCtr];

                            if (nodeAttribute.name.toLowerCase() == word.toLowerCase()) {
                                return modelNode;
                            }

                            // or attributes synonyms matches
                            var refid = model.modelgraphid + '||' + modelNode.nodeid + '||' + nodeAttribute.name.toLowerCase();
                            var attrsynonyms = this.GetSynonyms(refid);
                            // console.log('attrsynonyms=>', refid, attrsynonyms);
                            if (attrsynonyms && attrsynonyms != 'undefined' && attrsynonyms.length > 0) {
                                for (var synCtr = 0; synCtr < attrsynonyms.length; synCtr++) {
                                    if (attrsynonyms[synCtr].toLowerCase() == word.toLowerCase()) {
                                        return modelNode;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return null;
    }

    GetSynonyms(refid) {
        var syn = [];
        if (this.Synonyms && this.Synonyms.length > 0) {

            for (var i = 0; i < this.Synonyms.length; i++) {
                if (this.Synonyms[i].refid.toLowerCase() == refid.toLowerCase()) {
                    syn = this.Synonyms[i].synonyms;
                    break;
                }
            }
        }

        return syn;
    }


    GetNodeDatasetMappings(basemodelgraphid, baseid) {
        var self = this;
        var mappings = [];

        return new Promise(function (resolve, reject) {
            for (var i = 0; i < self.NodeDatasetMappings.length; i++) {
                //if base model matched then get the matched node
                if (self.NodeDatasetMappings[i].domainmodelgraphid == basemodelgraphid
                    && self.NodeDatasetMappings[i].nodeid == baseid) {
                    mappings.push(self.NodeDatasetMappings[i]);
                }
            }
            resolve(mappings);
        }); // Promise
    }

    GetEligibleDataset(datasetmappings, mappedConnector) {

        var self = this;
        var defaultconnector = {};

        return new Promise(function (resolve, reject) {
            if (datasetmappings && datasetmappings.length > 0) {
                for (var i = 0; i < datasetmappings.length; i++) {
                    var mapping = datasetmappings[i];

                    if (i == 0 || mapping.default) {
                        defaultconnector = mapping;
                    }

                    if (!self.CommonMethods.IsNull(mapping) && !self.CommonMethods.IsNull(mapping.datasetid)
                        && !self.CommonMethods.IsNull(mappedConnector) && !self.CommonMethods.IsNull(mappedConnector.datasetid)
                        && mapping.datasetid == mappedConnector.datasetid) {

                        mapping["name"] = mappedConnector.name;
                        resolve(mapping);
                        return;
                    }
                }
            }

            resolve(defaultconnector);
        }); // Promise
    }

    GetNodeProperty(model, nodeid) {
        if (model != null && model != 'undefined'
            && model.nodeproperties != null && model.nodeproperties != 'undefined') {
            // console.log('model.nodeproperties =>', model.nodeproperties);
            return this.CommonMethods.GetMatchedObject(model.nodeproperties, 'nodeid', nodeid);
        }
        return null;
    }

    ExecuteNodeConnector(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel) {
        var self = this;
        var connEntity = '';

        return new Promise(function (resolve, reject) {

            // Get the node - dataset mappings
            self.GetNodeDatasetMappings(intentffnode.basemodelgraphid, intentffnode.baseid)
                .then(datasetmappings => {
                    self.LogProgress('ExecuteNodeConnector - GetNodeDatasetMappings ...', datasetmappings, 1);
                    //var connectors = [];
                    var mappedConnector = {};

                    const intentffNodeProp = self.GetNodeProperty(intentFulfillmentModel, intentffnode.nodeid);
                    if (intentffNodeProp != null && intentffNodeProp.connectordataset != 'undefined') {
                        mappedConnector = intentffNodeProp.connectordataset;
                    }

                    // Get the applicable node - dataset mappings
                    self.GetEligibleDataset(datasetmappings, mappedConnector)
                        .then(connectordataset => {

                            self.LogProgress('Executing Node Connector...', connectordataset);
                            //execute the node data processor for that dataset
                            self.NodeDataProcessor.Execute(intentffnode, rowno, previousNodeDataRows,
                                nlpOutputElements, intentFulfillmentModel, connectordataset).then(
                                    result => {
                                        self.LogProgress('Node Connector executed...', intentffnode.label);
                                        resolve(result);
                                    }
                                );

                        }); // GetEligibleConnector
                }); // GetDomainModelNodeById
        }); // Promise
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }

}

module.exports = new DomainModelProcessor();