const commonMethods = require('../../lib/commonmethods');
const connectorHandler = require('./connector.handler');

class NodeDataProcessor {

    constructor() {
        this.ConnectorHandler = connectorHandler;
        this.CommonMethods = commonMethods;
        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };
    }

    InitializeDataSets(connectorAPIEndPoint, logger, sessionId) {
        this.ConnectorHandler.Initialize(connectorAPIEndPoint, logger, sessionId);
        this.Logger = logger;
        this.SessionId = sessionId;
    }

    GetNodeProperty(model, nodeid) {
        if (model != null && model != 'undefined'
            && model.nodeproperties != null && model.nodeproperties != 'undefined') {
            // this.Logger.Log(this.SessionId, 'model.nodeproperties =>', model.nodeproperties);
            return this.CommonMethods.GetMatchedObject(model.nodeproperties, 'nodeid', nodeid);
        }
        return null;
    }

    GetParameters(rowno, previousNodeDataRows, intentffnode, nlpOutputElements, intentFulfillmentModel) {
        var self = this;
        var params = {};

        return new Promise(function (resolve, reject) {
            const nodeprop = self.GetNodeProperty(intentFulfillmentModel, intentffnode.nodeid);
            if (nodeprop != null) {
                for (var pCtr = 0; pCtr < nodeprop.inputparameters.length; pCtr++) {
                    params[nodeprop.inputparameters[pCtr].name] = self.ConnectorHandler.GetParamValue(rowno, previousNodeDataRows, nodeprop.inputparameters[pCtr], nlpOutputElements, intentFulfillmentModel);
                }
            }
            resolve(params);
        });
    }

    TransformParams(connector, modelparams) {
        var params = modelparams;
        var self = this;

        return new Promise(function (resolve, reject) {
            if (modelparams != null && modelparams != 'undefined'
                && connector.attributemapping && connector.attributemapping.length > 0) {
                params = {};
                for (var keyCtr = 0; keyCtr < Object.keys(modelparams).length; keyCtr++) {
                    for (var attrCtr = 0; attrCtr < connector.attributemapping.length; attrCtr++) {
                        var modelColName = connector.attributemapping[attrCtr].nodeattr;
                        var dataColName = connector.attributemapping[attrCtr].connectorattr;
                        if (Object.keys(modelparams)[keyCtr] == modelColName) {
                            params[dataColName] = modelparams[modelColName];
                        }
                    }
                }
            }
            self.LogProgress('transform params', params, 1);
            resolve(params);
        });
    }

    TransformData(connector, datafromsource) {
        var data = [];

        return new Promise(function (resolve, reject) {
            if (datafromsource && datafromsource.length > 0
                && connector.attributemapping && connector.attributemapping.length > 0) {

                for (var rowCtr = 0; rowCtr < datafromsource.length; rowCtr++) {
                    var datarow = {};
                    for (var attrCtr = 0; attrCtr < connector.attributemapping.length; attrCtr++) {
                        var modelColName = connector.attributemapping[attrCtr].nodeattr;
                        var dataColName = connector.attributemapping[attrCtr].connectorattr;

                        datarow[modelColName] = datafromsource[rowCtr][dataColName];
                    }
                    data.push(datarow);
                }
            }
            resolve(data);
        });
    }

    Execute(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel, connectordataset) {
        var self = this;
        var connEntityId = connectordataset.datasetid;
        var connEntityName = connectordataset.name;

        self.LogProgress('Execute connector dataset: ', JSON.stringify(connectordataset), 1);

        return new Promise(function (resolve, reject) {

            self.LogProgress('Execute connector dataset: ', 'ExecuteNodeConnector - rowno => ' + rowno + ' ' + JSON.stringify(previousNodeDataRows), 1);
            self.GetParameters(rowno, previousNodeDataRows, intentffnode, nlpOutputElements, intentFulfillmentModel)
                .then(connParams => {

                    self.TransformParams(connectordataset, connParams)
                        .then(connDataParams => {

                            self.LogProgress('Executing Node Data', { 'fetching data for connEntityId': connEntityId, 'entity': connEntityName, 'params': connDataParams }, 1);
                            self.ConnectorHandler.GetDataFromConnector(connEntityId, connEntityName, connDataParams, 0)
                                .then(response => {

                                    self.TransformData(connectordataset, response.data)
                                        .then(data => {
                                            self.LogProgress('Transform Data', connectordataset.name, 0);
                                            resolve({ error: response.error, value: data });
                                        })
                                }); //GetDataFromConnector
                        }); //TransformParams
                }); // GetParameters
        }); // Promise
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }

}

module.exports = new NodeDataProcessor();