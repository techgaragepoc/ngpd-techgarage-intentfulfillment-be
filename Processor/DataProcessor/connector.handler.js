
const request = require('request');
const commonMethods = require('../../lib/commonmethods');

class ConnectorHandler {

    constructor() {
        this.CommonMethods = commonMethods;
        this.AdminAPIEndPoint = '';
        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };
    }

    Initialize(connectorAPIEndPoint, logger, sessionId) {
        this.AdminAPIEndPoint = connectorAPIEndPoint;
        this.Logger = logger;
        this.SessionId = sessionId;
    }

    GetParamValue(rowno, previousNodeDataRows, paramsrc, nlpOutputElements, intentFulfillmentModel) {
        if (paramsrc.sourcetype == 'intententity') {
            for (var i = 0; i < nlpOutputElements.Entities.length; i++) {
                if (nlpOutputElements.Entities[i].entity_type.toLowerCase() == paramsrc.sourcename.toLowerCase()) {
                    return nlpOutputElements.Entities[i].value;
                }
            }
        }

        if (paramsrc.sourcetype == 'node') {
            for (var i = 0; i < intentFulfillmentModel.nodes.length; i++) {
                if (intentFulfillmentModel.nodes[i].nodeid == paramsrc.sourcename) {
                    var ds = this.CommonMethods.GetDataRowValue(intentFulfillmentModel.nodes[i].data, previousNodeDataRows, intentFulfillmentModel.nodes[i].nodeid);
                    //console.log('ds=>', {data: intentFulfillmentModel.nodes[i].data, rowno: rowno});
                    if (ds != null && ds != 'undefined' && ds.length > rowno
                        && ds[rowno][paramsrc.sourcevalue] != 'undefined') {
                        return ds[rowno][paramsrc.sourcevalue];
                    }
                    else {
                        return null;
                    }
                }
            }
        }
    }

    GetDataFromConnector(entityid, entityname, parameters, maxrows) {
        var self = this;

        return new Promise(function (resolve, reject) {

            var hdrdata = {
                entityid: entityid,
                entity: entityname,
                parameters: parameters,
                maxrows: maxrows
            };

            var url = self.AdminAPIEndPoint + '/ec/data';
            var options = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'reqdata': JSON.stringify(hdrdata)
                },
                json: true,
            };

            // console.log('request url options =>', {url :url, options: options});
            request(url, options, function (err, response, body) {
                // console.log('reponse from ec...',JSON.parse(body));
                if (err) {
                    resolve({ error: err, data: null });
                }
                else {
                    console.log('Execute REST Api Enterprise Connector (' + (entityname || entityid) + ')', 'statuscode=' + response.statusCode);
                    self.LogProgress('Enterprie Connector REST API Executed', { entity: (entityname || entityid), statuscode: response.statusCode },0);
                    if (response.statusCode >= 400) {
                        // callback(response.statusMessage, null);
                        resolve({ error: response.statusMessage, data: null });
                    }
                    else {
                        var result = self.CommonMethods.AsObject(body);
                        resolve({ error: null, data: result.data });
                        // callback(null, result.data);
                    }
                }
            });
        }); // Promise
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }

}

module.exports = new ConnectorHandler();