const IntentFulfillmentModelProcessor = require('../../Processor/IntentFulfillmentModel/intentfulfillmentmodel.processor');
const DomainModelProcessor = require('../../Processor/DomainModel/domainmodel.processor');
const FunctionProcessor = require('../../Processor/Function/function.processor');

const nlpPostProcessor = require('./nlp.postprocessor');

class IntentFulfillmentIdentifier {

    constructor() {
        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };

    }

    Initialize(domainmodels, intentfulfillmentmodels, aggfunctions, synonyms, nlpIntentMapping,
        nodedatasetmapping, connectorAPIEndPoint, logger, sessionId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.intentFulfillmentModelProcessor = IntentFulfillmentModelProcessor;
            self.domainModelProcessor = DomainModelProcessor;
            self.functionProcessor = FunctionProcessor;
            self.Logger = logger;
            self.SessionId = sessionId;

            self.intentFulfillmentModelProcessor.InitializeDataSets(domainmodels, intentfulfillmentmodels, aggfunctions, synonyms, nlpIntentMapping, nodedatasetmapping, connectorAPIEndPoint, logger, sessionId);
            self.domainModelProcessor.InitializeDataSets(domainmodels, synonyms, nodedatasetmapping, connectorAPIEndPoint, logger, sessionId);
            self.functionProcessor.InitializeDataSets(aggfunctions, logger, sessionId);

            resolve();
        });

    }

    GetMappedIntentFulfillmentModel(nlpModel, nlpIntent) {
        return this.intentFulfillmentModelProcessor.GetMappedIntentFulfillmentModelGraphId(nlpModel, nlpIntent)
            .then(modelgraphid => {
                // console.log('GetMappedIntentFulfillmentModel modelgraphid', modelgraphid);
                this.LogProgress('Identified the intentffmodel (graphid)', modelgraphid, 0);
                return this.intentFulfillmentModelProcessor.GetIntentFulfillmentModel(modelgraphid);
            }); // GetMappedIntentModelId
    }

    ProcessIntentFulfillmentModel(nlpTestUtterance, intentFulfillmentModel) {
        return this.intentFulfillmentModelProcessor.ProcessIntentFulfillmentModel(nlpTestUtterance, intentFulfillmentModel);
    }


    //Simulator Methods
    GetKeywords(nlpOutput) {
        var keywords = [];

        var nouns = nlpPostProcessor.GetIntentNouns(nlpOutput);
        var propnouns = nlpPostProcessor.GetIntentProperNouns(nlpOutput);
        var verbs = nlpPostProcessor.GetIntentVerbs(nlpOutput);
        var adjectives = nlpPostProcessor.GetIntentAdjectives(nlpOutput);
        var entities = nlpPostProcessor.GetIntentEntities(nlpOutput);
        var POS = nlpPostProcessor.GetIntentPOS(nlpOutput);

        // console.log('pronouns...adj', propnouns, adjectives);
        self.LogProgress('Keywords extracted from POS (pronouns and adjectives)', { pronouns: propnouns, adj: adjectives }, 1);

        //include prop noun
        for (var pnCtr = 0; pnCtr < propnouns.length; pnCtr++) {
            keywords.push(propnouns[pnCtr]);
        }

        //include adjective
        for (var adjCtr = 0; adjCtr < adjectives.length; adjCtr++) {
            keywords.push(adjectives[adjCtr]);
        }

        //replace prop noun with identified entity
        for (var entCtr = 0; entCtr < entities.length; entCtr++) {
            for (var kwCtr = 0; kwCtr < keywords.length; kwCtr++) {
                if (entities[entCtr].value == keywords[kwCtr]) {
                    keywords[kwCtr] = entities[entCtr].entity_type;
                }
            }
        }

        return keywords;
    }

    /**
   *  This method will return the matched intent model based on keywords[] provided
   */
    GetIntentFulfillmentModel(nlpOutput) {
        var self = this;
        return new Promise(function (resolve, reject) {

            self.LogProgress('Identifying matched model', '', 0);

            var keywords = self.GetKeywords(nlpOutput);
            // console.log('keywords =>', keywords);
            if (keywords == null || keywords.length <= 0) {
                self.LogProgress('$$Aborted$$', 'Stopped: As no keywords extarcted', 0);
                return resolve(null);
            }
            else {

                var identifiedmappings = [];
                var intentFulfillmentModels = self.intentFulfillmentModelProcessor.GetIntentFulfillmentModels();

                //Get respective model or function againt each keyword
                for (var i = 0; i < keywords.length; i++) {
                    var model = self.domainModelProcessor.GetMatchedDomainModelNode(keywords[i]);
                    var func = self.functionProcessor.GetMatchedFunction(keywords[i]);
                    self.LogProgress('keywords - model - func =>', { keywords: keywords[i], model: model, functions: func }, 1);
                    identifiedmappings.push({ keyword: keywords[i], model: model, aggfunction: func });
                }

                self.LogProgress('identifiedmappings =>', identifiedmappings, 1);
                self.LogProgress('intentFulfillmentModels =>', intentFulfillmentModels, 1);

                //identify intent
                for (var modelCtr = 0; modelCtr < intentFulfillmentModels.length; modelCtr++) {
                    var intentfulfillmentmodel = intentFulfillmentModels[modelCtr];
                    var intentfulfillmentmodelfound = false;

                    if (intentfulfillmentmodel && intentfulfillmentmodel != 'undefined'
                        && intentfulfillmentmodel.details
                        && intentfulfillmentmodel.details.matchcriteria) {

                        var matched = false;
                        var matchedprop = 0;

                        for (var criteriaCtr = 0; criteriaCtr < intentfulfillmentmodel.details.matchcriteria.length; criteriaCtr++) {
                            var criteria = intentfulfillmentmodel.details.matchcriteria[criteriaCtr];

                            for (var mapCtr = 0; mapCtr < identifiedmappings.length; mapCtr++) {
                                //if matched with model
                                if (identifiedmappings[mapCtr].model != null
                                    && identifiedmappings[mapCtr].model.label.toLowerCase() == criteria.toLowerCase()) {
                                    // matched = true;
                                    matchedprop++;
                                }
                                else {

                                    //if matched with function
                                    if (identifiedmappings[mapCtr].aggfunction != null
                                        && identifiedmappings[mapCtr].aggfunction.name.toLowerCase() == criteria.toLowerCase()) {
                                        //  matched = true;
                                        matchedprop++;
                                    }
                                    else {
                                        if (identifiedmappings[mapCtr].keyword != null
                                            && identifiedmappings[mapCtr].keyword.toLowerCase() == criteria.toLowerCase()) {
                                            // matched = true;
                                            matchedprop++;
                                        }
                                    }
                                }
                                // console.log('criteria - model.name - aggfunction.name', criteria, identifiedmappings[mapCtr].model, identifiedmappings[mapCtr].aggfunction, matched);
                            }
                        }   //for all matching criteria of specific intent model 

                        self.LogProgress('intentfulfillmentmodel._id | matchedprop == identifiedmappings.length', { modelid: intentfulfillmentmodel._id, property: matchedprop, mappinglength: identifiedmappings.length }, 1);
                        if (matchedprop == identifiedmappings.length) {
                            matched = true;
                        }

                        if (!matched) {
                            intentfulfillmentmodelfound = false;
                            //break;  //if any of the mapped model or function is not present in intent model then check another intent model
                        }
                        else {
                            intentfulfillmentmodelfound = true;
                        }

                    }

                    if (intentfulfillmentmodelfound) {
                        self.LogProgress('matched model found', intentfulfillmentmodel.name, 0);
                        resolve(intentfulfillmentmodel);
                    }
                }  //for all intent models
            }
        }); //Promise
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }

}

module.exports = new IntentFulfillmentIdentifier();


