class NLPPostProcessor {

    constructor() {

    }

    GetIntentNouns(intentTree) {
        var list = [];

        if (intentTree && intentTree.Nouns) {
            for (var i = 0; i < intentTree.Nouns.length; i++) {
                list.push(intentTree.Nouns[i].value);
            }
        }
        return list;
    }

    GetIntentProperNouns(intentTree) {
        var list = [];

        if (intentTree && intentTree.Nouns) {
            for (var i = 0; i < intentTree.Nouns.length; i++) {
                if (intentTree.Nouns[i].type == "PROPN") {
                    list.push(intentTree.Nouns[i].value);
                }
            }
        }

        return list;
    }

    GetIntentVerbs(intentTree) {
        var list = [];

        if (intentTree && intentTree.Verbs) {
            for (var i = 0; i < intentTree.Verbs.length; i++) {
                list.push(intentTree.Verbs[i].value);
            }
        }
        return list;
    }


    GetIntentEntities(intentTree) {
        var list = [];

        if (intentTree && intentTree.Entities) {
            for (var i = 0; i < intentTree.Entities.length; i++) {
                list.push({ entity_type: intentTree.Entities[i].entity_type, value: intentTree.Entities[i].value });
            }
        }

        return list;
    }

    GetIntentAdjectives(intentTree) {
        var list = [];

        if (intentTree && intentTree.ADJ) {
            for (var i = 0; i < intentTree.ADJ.length; i++) {
                list.push(intentTree.ADJ[i].value);
            }
        }

        return list;
    }

    GetIntentPOS(intentTree) {
        var list = [];

        if (intentTree && intentTree.POS) {
            for (var i = 0; i < intentTree.POS.length; i++) {
                list.push({ word: intentTree.POS[i].word, pos: intentTree.POS[i].pos });
            }
        }

        return list;
    }
}

module.exports = new NLPPostProcessor();