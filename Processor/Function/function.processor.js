//const MongoToJson = require('../../Database/ZService/MongoToJson');
const jsongroupby = require('json-groupby');
const commonMethods = require('../../lib/commonmethods');

class FunctionProcessor {

    constructor() {
        this.CommonMethods = commonMethods;
        this.AggregateFunctions = [];

        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };

    }

    InitializeDataSets(aggfunctions, logger, sessionId) {
        this.AggregateFunctions = aggfunctions;
        this.Logger = logger;
        this.SessionId = sessionId;
    }


    GetMatchedFunction(word) {
        for (var i = 0; i < this.AggregateFunctions.length; i++) {
            if (this.AggregateFunctions[i].name.toLowerCase() == word.toLowerCase()) {
                return this.AggregateFunctions[i];
            }

            for (var j = 0; j < this.AggregateFunctions[i].synonyms.length; j++) {
                if (this.AggregateFunctions[i].synonyms[j].toLowerCase() == word.toLowerCase()) {
                    return this.AggregateFunctions[i];
                }
            }
        }

        return null;
    }

    GetMatchedFunctionById(baseid) {
        return this.CommonMethods.GetMatchedObject(this.AggregateFunctions, "code", baseid);;
    }

    GetNodeProperty(intentFulfillmentModel, nodeid) {
        if (intentFulfillmentModel != null && intentFulfillmentModel != 'undefined'
            && intentFulfillmentModel.nodeproperties != null && intentFulfillmentModel.nodeproperties != 'undefined') {
            // console.log('intentFulfillmentModel.nodeproperties =>', intentFulfillmentModel.nodeproperties);
            return this.CommonMethods.GetMatchedObject(intentFulfillmentModel.nodeproperties, 'nodeid', nodeid);
        }
        return null;
    }

    GetFunctionImplementation(baseid) {
        var matchedfunc = this.GetMatchedFunctionById(baseid);

        if (matchedfunc != null) {
            return require('./functions/' + matchedfunc.implementation);
        }
        else {
            return {
                execute: function () {
                    return null;
                }
            };
        }
    }

    ExecuteFunction(intentnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var basefuncdata = self.GetMatchedFunctionById(intentnode.baseid);
            var basefunc = self.GetFunctionImplementation(intentnode.baseid);
            var nodeprop = self.GetNodeProperty(intentFulfillmentModel, intentnode.nodeid);

            var paramsrc = nodeprop.inputparameters[0];
            var aggdataset = [];


            //get dataset for function execution
            if (paramsrc.sourcetype == 'node') {
                for (var i = 0; i < intentFulfillmentModel.nodes.length; i++) {
                    if (intentFulfillmentModel.nodes[i].nodeid == paramsrc.sourcename) {
                        aggdataset = self.CommonMethods.GetDataRowValue(intentFulfillmentModel.nodes[i].data, previousNodeDataRows, intentFulfillmentModel.nodes[i].nodeid);
                    }
                }
            } self.LogProgress('Executing function...', basefuncdata.name);

            //execute the function
            //var funcValue = basefunc.execute(aggdataset, paramsrc.sourcevalue, paramsrc.sourcegroupby);
            var funcValue = self.CallBaseFunction(aggdataset, paramsrc.name, paramsrc.sourcevalue, paramsrc.sourcegroupby,
                basefuncdata.name, basefunc.execute);

            self.LogProgress('Function Executed, result...', funcValue);

            resolve({ error: null, value: funcValue });
        });
    }

    CallBaseFunction(dataset, colname, col, groupbycols, funcname, func) {
        var group = [];
        var results = [];

        if (dataset && dataset.length > 0 && col) {
            // console.log('groubycols => ', groupbycols);
            if (groupbycols && groupbycols != 'undefined' && groupbycols.length > 0) {
                group = jsongroupby(dataset, groupbycols);
            }
            else {
                group = { 'main': dataset };
            }
            this.ProcessGroup(funcname, group, colname, col, 0, {}, groupbycols, func, results);
        }

        return results;
    }


    ProcessGroup(funcname, group, colname, col, level, groupbykey, groupbycols, func, resultarray) {

        for (var grp = 0; grp < Object.keys(group).length; grp++) {
            var key = Object.keys(group)[grp];
            var val = group[key];
            var result = {};

            if (groupbycols && groupbycols != 'undefined' && groupbycols.length > 0) {
                groupbykey[groupbycols[level]] = key;
                //  groupbykey = key;
            }
            else {
                groupbykey[level] = key;
            }

            result["GroupBy"] = groupbykey;
            result["AggregateFunction"] = funcname;
            result["AggregateColumn"] = col;
            result[colname] = 0;

            if (val && typeof val === 'object') {
                if (val instanceof Array) {
                    result[colname] = func(val, col);
                    resultarray.push(result);
                    groupbykey = {};
                }
                else {
                    processgroup(funcname, val, colname, col, eval(level + 1), groupbykey, groupbycols, resultarray)
                }
            }
            else {
                resultarray.push(result);
            }
        }
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }

}

module.exports = new FunctionProcessor();