module.exports = {
    execute: function(dataset, col) {
        var max = 0; 
        if (dataset && dataset.length>0 && col) {
             for(var i=0; i<dataset.length; i++) {
                  if (dataset[i][col] && dataset[i][col] != 'undefined') {
                      if (i==0 || max < dataset[i][col]) {
                        max = dataset[i][col];
                      }
                  }
             }
        }

        return max;
    }
}