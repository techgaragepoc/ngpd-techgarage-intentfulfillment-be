module.exports = {
    execute: function(dataset, col) {
        var sum = 0; 
        if (dataset && dataset.length>0 && col) {
             for(var i=0; i<dataset.length; i++) {
                  if (dataset[i][col] && dataset[i][col] != 'undefined') {
                     sum += dataset[i][col];    
                  }
             }
        }

        return sum;
    }
}