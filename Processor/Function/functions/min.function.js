module.exports = {
    execute: function(dataset, col) {
        var min = 0; 
        if (dataset && dataset.length>0 && col) {
             for(var i=0; i<dataset.length; i++) {
                
                  if (dataset[i][col] && dataset[i][col] != 'undefined') {
                      if (i==0 || min > dataset[i][col]) {
                          min = dataset[i][col];
                      }
                  }
             }
        }

        return min;
    }
}