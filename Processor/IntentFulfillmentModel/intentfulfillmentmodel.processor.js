const domainModelProcessor = require('../DomainModel/domainmodel.processor');
const functionProcessor = require('../Function/function.processor');
const Clone = require('clone');
const CommonMethods = require('../../lib/commonmethods');
const resultNodeHandler = require('./resultnode.handler');

class IntentFulfillmentModelProcessor {

    constructor() {
        this.DomainModelProcessor = domainModelProcessor;
        this.FunctionProcessor = functionProcessor;
        this.ResultNodeHandler = resultNodeHandler;
        this.IntentFulfillmentModels = [];
        this.NLPIntentFulfillmentMapping = [];

        this.loopCounter = 0;

        this.SessionId = '';
        this.Logger = {
            Log: function (sessionid, message, level = 0) {
                console.log('logger not assigned properly - ' + level + ': ' + message);
            }
        };

    }

    InitializeDataSets(domainmodels, intentfulfillmentmodels, aggfunctions, synonyms,
        nlpIntentFulfillmentMapping, nodedatasetmapping, connectorAPIEndPoint, logger, sessionId) {
        this.IntentFulfillmentModels = intentfulfillmentmodels;
        this.DomainModelProcessor.InitializeDataSets(domainmodels, synonyms, nodedatasetmapping, connectorAPIEndPoint, logger, sessionId);
        this.FunctionProcessor.InitializeDataSets(aggfunctions, logger, sessionId);
        this.NLPIntentFulfillmentMapping = nlpIntentFulfillmentMapping;
        this.Logger = logger;
        this.SessionId = sessionId;
    }

    GetIntentFulfillmentModels() {
        return this.IntentFulfillmentModels;
    }

    GetIntentFulfillmentModel(intentfulfillmentmodelgraphid) {
        var self = this;
        return new Promise(function (resolve, reject) {

            if (intentfulfillmentmodelgraphid && intentfulfillmentmodelgraphid != 'undefined') {
                var model = CommonMethods.GetMatchedObject(self.IntentFulfillmentModels, 'modelgraphid', intentfulfillmentmodelgraphid);

                if (model != null && model != 'undefined') {
                    // // console.log('model =>', model);
                    resolve(model);
                }
                else {
                    resolve(null);
                }
            }
            else {
                resolve(null);
            }
        });
    }

    GetMappedIntentFulfillmentModelGraphId(nlpModel, nlpIntent) {
        // console.log('this.NLPIntentFulfillmentMapping =>', this.NLPIntentFulfillmentMapping);
        // console.log('nlpModel, nlpIntent =>', {nlpmodel: nlpModel, nlpintent: nlpIntent});

        var self = this;
        return new Promise(function (resolve, reject) {
            var mappedintentfulfillmentmodelgraphid = null;
            for (var i = 0; i < self.NLPIntentFulfillmentMapping.length; i++) {
                if (self.NLPIntentFulfillmentMapping[i].nlpintentdetail.model == nlpModel && self.NLPIntentFulfillmentMapping[i].nlpintentdetail.intent == nlpIntent) {
                    mappedintentfulfillmentmodelgraphid = self.NLPIntentFulfillmentMapping[i].intentfulfillmentmodelgraphid;
                    break;
                }
            }

            if (mappedintentfulfillmentmodelgraphid != null && mappedintentfulfillmentmodelgraphid != 'undefined') {
                resolve(mappedintentfulfillmentmodelgraphid);
            }
            else {
                resolve(null);
            }
        });
    }

    GetIntentFulfillmentNode(nodeid, intentmodel) {
        for (var i = 0; i < intentmodel.nodes.length; i++) {
            if (intentmodel.nodes[i].nodeid == nodeid) {
                return intentmodel.nodes[i];
            }
        }
        return null;
    }

    GetIntentFulfillmentNodeProperties(nodeid, intentmodel) {
        for (var i = 0; i < intentmodel.nodeproperties.length; i++) {
            if (intentmodel.nodeproperties[i].nodeid == nodeid) {
                return intentmodel.nodeproperties[i];
            }
        }
        return null;
    }


    ProcessIntentFulfillmentModel(nlptree, intentFulfillmentModel) {
        var self = this;
        self.loopCounter = 0;
        return new Promise(function (resolve, reject) {
            // Validation 1: if intent model is null or undefined
            if (intentFulfillmentModel == null || intentFulfillmentModel == 'undefined') {
                self.LogProgress('Process IntentFulfillment Model', 'intentFulfillmentModel is either null or undefined');
                resolve({
                    error: { message: 'intentFulfillmentModel is either null or undefined' },
                    processedintentmodel: null,
                    resultnode: null,
                    resultrows: []
                });
            }
            else {
                self.LogProgress('Process IntentFulfillment Model', 'Start Processing Model');
                // Validation 2: if start node is not defined
                var startnode = self.GetIntentFulfillmentNode(intentFulfillmentModel.details.startnode, intentFulfillmentModel);
                // console.log('start node=>', startnode);

                if (startnode == null && startnode == 'undefined') {
                    self.LogProgress('$$Aborted$$', 'Stopped: As startnode not found');
                    resolve({
                        error: { message: 'Stopped: As startnode not found' },
                        processedintentmodel: null,
                        resultnode: null,
                        resultrows: []
                    });
                }
                else {
                    // Process Start Node
                    self.LogProgress('Start Node', startnode.label);
                    var previousNodeDataRows = [];
                    self.ProcessNode(startnode, 0, previousNodeDataRows, nlptree, intentFulfillmentModel)
                        .then(processedintentmodel => {
                            //   function (processedintentmodel) {
                            var resultnode = self.GetIntentFulfillmentNode(processedintentmodel.details.resultnode, processedintentmodel);
                            resultnode["properties"] = self.GetIntentFulfillmentNodeProperties(processedintentmodel.details.resultnode, processedintentmodel);

                            var filteredRows = self.ResultNodeHandler.GetResultNodeRows(resultnode);
                            // console.log('processed model =>', processedintentmodel);
                            // console.log('result node data =>', resultnode.data);

                            resolve({
                                error: null,
                                processedintentmodel: processedintentmodel,
                                resultnode: resultnode,
                                resultrows: filteredRows
                            });
                        }); // ProcessNode
                }
            }
        }); // Promise
    }

    ProcessNode(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel) {
        var self = this;

        return new Promise(function (resolve, reject) {
            // var intentffnode = self.GetIntentNode(nodeid, intentFulfillmentModel);
            if (intentffnode) {
                self.GetNodeValue(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel)
                    .then(result => {
                        if (intentffnode.nextnodes && intentffnode.nextnodes.length > 0) {
                            var priorNodeDataLinks = Clone(previousNodeDataRows);
                            priorNodeDataLinks.push({
                                nodeid: intentffnode.nodeid,
                                rowno: rowno
                            });

                            for (var nxtndCtr = 0; nxtndCtr < intentffnode.nextnodes.length; nxtndCtr++) {
                                var nextNode = self.GetIntentFulfillmentNode(intentffnode.nextnodes[nxtndCtr], intentFulfillmentModel);

                                //if nextnode is entity then execute nextnode for each result row
                                if (nextNode.nodetype.toLowerCase() == 'entity') {
                                    //when there is output of node execution
                                    if (result != null && result != 'undefined' && result.length > 0) {
                                        for (var outputRowCtr = 0; outputRowCtr < result.length; outputRowCtr++) {
                                            //// console.log('outputRowCtr => ',outputRowCtr);
                                            self.loopCounter++;
                                            self.ProcessNode(nextNode, outputRowCtr, priorNodeDataLinks, nlpOutputElements, intentFulfillmentModel)
                                                .then(model => {

                                                    // console.log('one process ends here =>', {loopctr: self.loopCounter, model: model});
                                                    //  self.loopCounter--;
                                                    if (self.loopCounter <= 1) {
                                                        resolve(model);
                                                    }
                                                });
                                        }
                                    }
                                    else {
                                        self.loopCounter++;
                                        //when there is no output of node execution then still call the next nodes
                                        self.ProcessNode(nextNode, 0, priorNodeDataLinks, nlpOutputElements, intentFulfillmentModel)
                                            .then(model => {
                                                // console.log('one process ends here =>', {loopctr: self.loopCounter, model: model});
                                                // self.loopCounter--;
                                                if (self.loopCounter <= 1) {
                                                    resolve(model);
                                                }
                                            });
                                    }
                                }
                                else {
                                    self.loopCounter++;

                                    //if aggregate function then all rows get converge 
                                    self.ProcessNode(nextNode, 0, priorNodeDataLinks, nlpOutputElements, intentFulfillmentModel)
                                        .then(model => {
                                            // console.log('one process ends here =>', {loopctr: self.loopCounter, model: model});

                                            if (self.loopCounter <= 1) {
                                                resolve(model);
                                            }
                                        });
                                }
                            }
                        }
                        else {
                            ///when no next node to process
                            self.loopCounter--;
                            // console.log('ProcessNode','when no next node to process');
                            resolve(intentFulfillmentModel);
                            // return;
                        }
                    });
            }
            else {
                ///when intentffnode is undefined or invalid
                // console.log('ProcessNode','when intentffnode is undefined or invalid');
                resolve(intentFulfillmentModel);
            }
        }); // Promise
    }

    GetNodeValue(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel) {

        var self = this;
        return new Promise(function (resolve, reject) {

            if (intentffnode.nodetype && intentffnode.nodetype != 'undefined') {
                if (intentffnode.nodetype.toLowerCase() == 'entity') {
                    self.DomainModelProcessor.ExecuteNodeConnector(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel)
                        .then(result => {
                            self.LogProgress('data fecthed for ' + intentffnode.label + ' => ', result.value, 1);
                            self.SetIntentNodeData(intentffnode, rowno, previousNodeDataRows, result.value, result.error);
                            resolve(result.value);
                            return;
                        });
                }
                else {
                    if (intentffnode.nodetype.toLowerCase() == 'aggregatefunction') {
                        self.FunctionProcessor.ExecuteFunction(intentffnode, rowno, previousNodeDataRows, nlpOutputElements, intentFulfillmentModel)
                            .then(result => {
                                self.LogProgress('function ' + intentffnode.label + ' value: ', result.value, 1);
                                self.SetIntentNodeData(intentffnode, rowno, previousNodeDataRows, result.value, result.error);
                                resolve(result.value);
                                return;
                            });
                    }
                }
            }
            else {
                resolve(null);
                return;
            }
        }); // Promise
    }

    SetIntentNodeData(intentffnode, rowno, previousNodeDataRows, result, err) {
        if (intentffnode["data"] == 'undefined' || intentffnode["data"] == null) {
            intentffnode["data"] = [];
        }

        if (result != null && result != 'undefined') {
            intentffnode["data"].push({ rowno: rowno, value: result, priornodedatalinks: previousNodeDataRows });
        }

        if (err != null && err != 'undefined') {
            if (intentffnode["error"] == 'undefined' || intentffnode["error"] == null) {
                intentffnode["error"] = [];
            }

            intentffnode["error"].push({ rowno: rowno, value: err });
        }
    }

    LogProgress(step, message, level = 0) {
        var messageString = (typeof message === 'object') ? JSON.stringify(message) : message;
        this.Logger.Log(this.SessionId, {
            step: step,
            message: messageString
        }, level);
    }
}

module.exports = new IntentFulfillmentModelProcessor();