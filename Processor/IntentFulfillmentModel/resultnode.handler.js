const CommonMethods = require('../../lib/commonmethods');

class ResultNodeHandler {

    constructor() {
        this.CommonMethods = CommonMethods;
    }

    GetRows(dataArray, result) {
        if (dataArray != null && dataArray != 'undefined' && dataArray.length > 0) {
            for (var i = 0; i < dataArray.length; i++) {
                if (this.CommonMethods.IsArray(dataArray[i])) {
                    getRows(dataArray[i], result);
                }
                else {
                    result.push(dataArray[i]);
                }
            }
        }
        return;
    }

    GetResultNodeRows(resultnode) {
        var result = [];

        if (resultnode != null && resultnode != 'undefined'
            && resultnode.data != null && resultnode.data != 'undefined' && resultnode.data.length > 0) {
            var data = [];

            //consolidate the result rows
            for (var i = 0; i < resultnode.data.length; i++) {
                this.GetRows(resultnode.data[i].value, data);
            }
            // console.log('intial data =>', JSON.stringify(data));

            if (resultnode.properties != null && resultnode.properties != 'undefined') {
                var outputColumns = resultnode.properties.output;


                if (outputColumns != null && outputColumns != 'undefined' && outputColumns.length > 0) {
                    outputColumns = outputColumns.join('|').toLowerCase().split('|');
                    //console.log('outputColumns =>', resultnode.properties.output, JSON.stringify(outputColumns));
                    //console.log('data length =>', data.length);
                    for (var rowCtr = 0; rowCtr < data.length; rowCtr++) {
                        var outputRow = {};
                        for (var colCtr = 0; colCtr < Object.keys(data[rowCtr]).length; colCtr++) {
                            var colName = Object.keys(data[rowCtr])[colCtr];
                            if (outputColumns.indexOf(colName.toLowerCase()) > -1) {
                                outputRow[colName] = data[rowCtr][colName];
                            }
                        }
                        result.push(outputRow);
                    }
                }
                else { // if output columns are not defined throw the default resultset
                    result = data;
                }
            }
            else { // if properties are not defined throw the default resultset
                result = data;
            }
        }

        return result;
    }

}

module.exports = new ResultNodeHandler();