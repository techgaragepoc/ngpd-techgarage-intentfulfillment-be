class TransformGraphModelHandler {
    constructor() {

    }

    GetDomainModel(domainModelGraph) {
        const domainModelGraphId = domainModelGraph._id;
        const domainModelName = domainModelGraph.name;
        const domainModelGraphJson = domainModelGraph.graphjson;
        const domainModelNodeProperties = domainModelGraph.nodeproperties;

        return new Promise(function (resolve, reject) {
            try {
                const nodesWithLinks = getNodeWithLinkDetails(getNodes('domain', domainModelGraphJson), getEdges(domainModelGraphJson));
                const nodeprop = getDomainModelNodeProperties(nodesWithLinks, domainModelNodeProperties);

                resolve({
                    "modelgraphid": domainModelGraphId,
                    "name": domainModelName,
                    "nodes": nodesWithLinks,
                    "nodeproperties": nodeprop
                });
            }
            catch (e) {
                reject(e);
            }
        });
    }

    GetIntentffModel(intentffModelGraph) {
        const intentffModelGraphId = intentffModelGraph._id;
        const intentffModelName = intentffModelGraph.name;
        const intentffGraphJson = intentffModelGraph.graphjson;
        const intentffNodeProperties = intentffModelGraph.nodeproperties;

        return new Promise(function (resolve, reject) {
            try {
                const nodesWithLinks = getNodeWithLinkDetails(getNodes('intentff', intentffGraphJson), getEdges(intentffGraphJson));
                const nodeprop = getIntentffNodeProperties(nodesWithLinks, intentffNodeProperties);
                const intentffModelDetails = getIntentffModelDetails(nodesWithLinks, nodeprop)

                console.log('nodesWithLinks =>', nodesWithLinks, nodeprop, intentffModelDetails);

                resolve({
                    "modelgraphid": intentffModelGraphId,
                    "name": intentffModelName,
                    "details": intentffModelDetails,
                    "nodes": nodesWithLinks,
                    "nodeproperties": nodeprop
                });
            }
            catch (e) {
                reject(e);
            }
        });
    }
}


function getNodes(modeltype, graphjson) {
    var nodes = [];
    console.log('graphjson =>', graphjson);
    if (graphjson != null && graphjson != 'undefined'
        && IsArray(graphjson.node)) {

        for (var i = 0; i < graphjson.node.length; i++) {
            var graphnode = graphjson.node[i];
            if (graphnode.key != 'undefined') {
                var node = {};

                if (modeltype == 'intentff') {
                    node["domainmodelgraphid"] = graphnode.model;
                    node["baseid"] = graphnode.basekey;
                }

                node["nodeid"] = graphnode.key;
                node["nodetype"] = getNodeType(graphnode.nodetype, graphnode.figure);
                node["label"] = graphnode.text;

                nodes.push(node);
            }
        }
    }
    return nodes;
}

function getNodeType(nodetype, nodefigure) {
    var transformNodeType = "Entity";

    if (nodetype!=null && nodetype != 'undefined') {
        switch (nodetype.toLowerCase()) {
            case "entity": transformNodeType = "Entity"; break;
            case "method": transformNodeType = "AggregateFunction"; break;
        }
    }
    else {
        if (nodefigure != null && nodefigure != 'undefined') {
            switch (nodefigure.toLowerCase()) {
                case "circle": transformNodeType = "Entity"; break;
                case "roundedrectangle": transformNodeType = "AggregateFunction"; break;
            }
        }
    }

    return transformNodeType;
}

function getEdges(graphjson) {
    var edges = [];
    if (graphjson != null && graphjson != 'undefined'
        && IsArray(graphjson.link)) {

        for (var i = 0; i < graphjson.link.length; i++) {
            if (graphjson.link[i].from != 'undefined' && graphjson.link[i].to != 'undefined') {
                var edge = {
                    from: graphjson.link[i].from,
                    to: graphjson.link[i].to,
                }
                edges.push(edge);
            }
        }
    }

    return edges;
}

function getNodeWithLinkDetails(nodes, edges) {
    if (IsArray(nodes) && nodes.length > 0
        && IsArray(edges) && edges.length > 0) {

        for (var nodeCtr = 0; nodeCtr < nodes.length; nodeCtr++) {
            nodes[nodeCtr]["startnode"] = true;
            nodes[nodeCtr]["endnode"] = true;
            nodes[nodeCtr]["nextnodes"] = [];
            for (var edgeCtr = 0; edgeCtr < edges.length; edgeCtr++) {
                if (nodes[nodeCtr].nodeid == edges[edgeCtr].from) {
                    nodes[nodeCtr].nextnodes.push((edges[edgeCtr].to));
                    nodes[nodeCtr]["endnode"] = false; //if any nextnode is assigned then it can't be end node
                }

                //if node is mentioned as 'to' for any edge then it can't be startnode
                if (nodes[nodeCtr].nodeid == edges[edgeCtr].to) {
                    nodes[nodeCtr]["startnode"] = false;
                }
            }
        }
    }

    return nodes;
}

function getDomainModelNodeProperties(nodeWithLinks, nodeProperties) {
    var modelNodeProperties = [];

    for (var i = 0; i < nodeProperties.length; i++) {
        var nodeprop = {
            "nodeid": nodeProperties[i].key,
            "attributes": nodeProperties[i].attributes,
            "connectors": nodeProperties[i].connectors,
        };

        modelNodeProperties.push(nodeprop);
    }
    return modelNodeProperties;
}

function getIntentffNodeProperties(nodeWithLinks, nodeProperties) {
    var modelNodeProperties = [];

    for (var i = 0; i < nodeProperties.length; i++) {
        var nodeprop = {
            "nodeid": nodeProperties[i].key,
            "connectorcondition": nodeProperties[i].connectorcondition,
            "inputparameters": nodeProperties[i].inputparameters,
            "output": nodeProperties[i].output
        };

        modelNodeProperties.push(nodeprop);
    }
    return modelNodeProperties;
}

function getIntentffModelDetails(nodeWithLinks, nodeProperties) {
    var modelDetails = {};
    var startnode = null;
    var resultnode = null;
    var matchcriteria = [];

    if (nodeWithLinks != null && nodeWithLinks != 'undefined' && nodeWithLinks.length > 0) {
        for (var i = 0; i < nodeWithLinks.length; i++) {
            if (nodeWithLinks[i]["startnode"] == true) {
                startnode = nodeWithLinks[i].nodeid;
            }

            if (nodeWithLinks[i]["endnode"] == true) {
                resultnode = nodeWithLinks[i].nodeid;
            }

            matchcriteria.push(nodeWithLinks[i].label);
        }
    }

    modelDetails["matchcriteria"] = matchcriteria;
    modelDetails["startnode"] = startnode;
    modelDetails["resultnode"] = resultnode;

    return modelDetails;
}

function IsArray(value) {
    return value && value != 'undefined' && typeof value === 'object' && value.constructor === Array;
}


module.exports = new TransformGraphModelHandler();